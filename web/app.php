<?php

//var_dump(posix_getpwuid(posix_geteuid()));die;

foreach (array('App/parameters.yml', 'cache', 'logs') as $path) {
    if (!is_writable(__DIR__ . '/../' . $path)) {
        die(sprintf('"%s" is not writable. Please change its permissions.', $path));
    }
}

require_once __DIR__ . '/../vendor/autoload.php';
$app = new Avris\Micrus\App('prod');
$app->run();