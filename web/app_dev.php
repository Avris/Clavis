<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || php_sapi_name() == 'cli-server'
    || !(in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', 'fe80::1', '::1')))
) {
    header('HTTP/1.0 403 Forbidden');
    exit('This file can only be access on localhost');
}

require_once __DIR__ . '/../vendor/autoload.php';
$app = new Avris\Micrus\App('dev');
$app->run();