mainPreloader = $('.preloader')
mainContent = $('main .content')
modalFolder = $('#modal-folder')
modalKey = $('#modal-key')
modalKeyChanges = $('#modal-key-changes')

treeoptions =
  plugins: [ 'types', 'wholerow' ]
  types: window.icons
  core:
    check_callback: true
    multiple: false

if window.isAdmin
  treeoptions.plugins.push 'contextmenu'
  treeoptions.contextmenu =
    select_node: false
    items: (o, cb) -> {
    newkey:
      label: "Create key"
      icon: "fa fa-plus"
      action: (data) ->
        inst = $.jstree.reference(data.reference)
        obj = inst.get_node(data.reference)
        addKey(if obj.id == 'personal' then '' else obj.id)
    create:
      label: "Create subfolder"
      icon: "fa fa-folder"
      _disabled: (data) ->
        inst = $.jstree.reference(data.reference)
        obj = inst.get_node(data.reference)
        return obj.id == 'personal'
      action: (data) ->
        inst = $.jstree.reference(data.reference)
        obj = inst.get_node(data.reference)
        createFolder obj.id
    edit:
      label: "Edit"
      icon: "fa fa-pencil"
      _disabled: (data) ->
        inst = $.jstree.reference(data.reference)
        obj = inst.get_node(data.reference)
        return obj.id == 'personal'
      action: (data) ->
        inst = $.jstree.reference(data.reference)
        obj = inst.get_node(data.reference)
        editFolder obj.id
    remove:
      label: 'Delete'
      icon: "fa fa-trash"
      _disabled: (data) ->
        inst = $.jstree.reference(data.reference)
        obj = inst.get_node(data.reference)
        return obj.id == 'personal' or obj.parent == '#'
      action: (data) ->
        inst = $.jstree.reference(data.reference)
        obj = inst.get_node(data.reference)
        removeFolder if inst.is_selected(obj) then inst.get_selected().join('') else obj.id
    }

$tree = $('#jstree').jstree treeoptions
.on 'changed.jstree', (e, data) ->
  if data and data.selected and data.selected.length
    loadFolder(data.selected.join(''))

loadFolder = (id) ->
  mainPreloader.show()
  $.get M.route('folder', {id: id}), (data) ->
    mainContent.html(data).show()
    mainPreloader.hide()
    bind()

reloadTree = ->
  selected = $tree.jstree('get_selected').join('')
  $tree.jstree('deselect_all').jstree('select_node', selected)

editFolder = (id) ->
  mainPreloader.show()
  $.get(M.route('folderEdit', {id: id}), [], (data) ->
    mainPreloader.hide()
    modalFolder.html(data).modal()
    setTimeout (-> modalFolder.find('input[type=text]')[0].focus()), 500
    bind()
  )

removeFolder = (id) ->
  return false if not confirm(M.l('Are you sure you want to remove this folder, its subfolders and keys?'))
  mainPreloader.show()
  $.post(M.route('folderRemove', {id: id}), [], (data) -> window.location.reload() )

createFolder = (parent) ->
  mainPreloader.show()
  $.get(M.route('folderAdd', {id: parent}), [], (data) ->
    mainPreloader.hide()
    modalFolder.html(data).modal()
    setTimeout (-> modalFolder.find('input[type=text]')[0].focus()), 500
    bind()
  )

unrememberPassword = ->
  user = $('#key_username');
  pass = $('#key_password');
  user.val('') if not user.attr('value')
  pass.val('') if not pass.attr('value')

addKey = (folder) ->
  mainPreloader.show()
  $.get M.route('keyAdd', {id: folder}), (data) ->
    mainPreloader.hide()
    modalKey.html(data).modal()
    setTimeout (-> modalKey.find('input[type=text]')[0].focus(); unrememberPassword()), 500
    bind()
  return false

bind = ->
  $('a.folder-link').unbind('click').click ->
    $tree.jstree('deselect_all').jstree('select_node', $(this).data('folder-id'))
    return false
  $('a.key-link').unbind('click').click ->
    mainPreloader.show()
    $.get M.route($(this).data('action'), {id: $(this).data('key-id')}), (data) ->
      mainPreloader.hide()
      modalKey.html(data).modal()
      setTimeout (-> modalKey.find('input[type=text]')[0].focus(); unrememberPassword()), 500
      bind()
    return false
  $('.key-changes-link').unbind('click').click ->
    mainPreloader.show()
    $.get M.route('keyChanges', {id: $(this).data('key-id')}), (data) ->
      modalKeyChanges.html(data).modal()
      mainPreloader.hide()
      bind()
    return false
  $('[data-toggle=tooltip]').tooltip()
  keyForm = modalKey.find('form')
  keyForm.unbind('submit').submit (e) ->
    if keyForm.isValid()
      mainPreloader.show()
      $.post(keyForm.attr('action'), keyForm.serializeObject(), (data) ->
        mainPreloader.hide()
        if data == 'OK'
          modalKey.modal('hide')
          reloadTree()
        else
          modalKey.html(data)
          bind()
      )
    e.preventDefault()
    return false
  $('.key-add').unbind('click').click ->
    addKey($(this).data('folder-id'))
  $('.folder-edit').unbind('click').click ->
    editFolder($(this).data('folder-id'))
  $('.folder-add').unbind('click').click ->
    createFolder($(this).data('folder-id'))
  $('.folder-remove').unbind('click').click ->
    removeFolder($(this).data('folder-id'))
  folderForm = modalFolder.find('form')
  folderForm.unbind('submit').submit (e) ->
    if folderForm.isValid()
      mainPreloader.show()
      $.post(folderForm.attr('action'), folderForm.serializeObject(), (data) ->
        if data == 'OK'
          window.location.reload()
        else
          modalFolder.html(data)
          bind()
      )
    e.preventDefault()
    return false

  $('.select2').select2()

  $('.key-remove').unbind('click').click ->
    return false if not confirm(M.l('Are you sure you want to remove this key?'))
    mainPreloader.show()
    $.post(M.route('keyRemove', {id: $(this).data('key-id')}), [], (data) -> window.location.reload() )

  userRole = $('select[name="user[role]"]')
  if userRole.length
    permissionForm = $('label[for$="_ownPermissionList"]').parent().parent()
    checkUserRole = ->
      if userRole.val() == 'ROLE_ADMIN'
        permissionForm.hide()
      else
        permissionForm.show()
    userRole.unbind('change').change(checkUserRole)
    checkUserRole()

  dbHider = ->
    $('.dbHide').parent().parent().hide()
    $('.'+$('#formdata_db_driver').val()).parent().parent().show()
  $('#formdata_db_driver').change(dbHider);
  dbHider();

  $('form.installer').submit ->
    if $(this).isValid()
      mainPreloader.show()
bind()

resize = ->
  mainPreloader.height(window.innerHeight)
$(window).resize(resize)
resize()

