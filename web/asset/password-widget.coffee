String::matchCount = (regex) -> matches = this.match regex; if matches == null then 0 else matches.length
String::randomize = (n) -> out = (this.charAt Math.floor Math.random() * this.length for [1..n]).join('')

$(document).ready ->
  $('.password-widget').each (i, el) ->
    widget = $(el)
    mainpass = widget.find('input.mainpass');
    pB = widget.find('.progress-bar')
    showHide = widget.find('.input-group-addon.showHide')
    generate = widget.find('.input-group-addon.generate')
    generator = widget.find('.generator')
    doGenerate = widget.find('button.do-generate')

    options = {}
    generator.find('[data-option]').each (i, el) -> el = $(el); options[el.data('option')] = el

    widget.find('.input-group-addon').css('cursor', 'pointer').css('user-select', 'none')

    showHide.click ->
      if mainpass.attr('type') == 'password'
        mainpass.attr('type', 'text')
        $(this).addClass('active')
      else
        mainpass.attr('type', 'password')
        $(this).removeClass('active')

      mainpass[0].setSelectionRange(0, mainpass.val().length)
      mainpass[0].focus()
      return false

    updatePb = (points) ->
      points = 5 if points < 5
      points = 100 if points > 100
      pB.css 'width', points+'%'
      pBclass = switch
        when points < 30 then 'progress-bar-danger'
        when points < 60 then 'progress-bar-warning'
        else 'progress-bar-success'
      pB.removeClass 'progress-bar-danger'
      .removeClass 'progress-bar-warning'
      .removeClass 'progress-bar-success'
      .addClass pBclass

    doEvaluatePassword = (pass) ->
      return 0 if pass.length < 6 or pass.toLowerCase() in ['password', 'administrator', '123456', '1234567', '12345678', '123456789', '1234567890']
      points = pass.length * 3
      points += 3 * pass.matchCount(/[A-Z]/g) if pass.matchCount(/[a-z]/g)
      points += 3 * pass.matchCount(/[0-9]/g) if pass.matchCount(/[A-Za-z]/g)
      points += 6 * pass.matchCount(/[`~!@#$%^&*()_\-+=\[\]\{};:?,.<>]/g)
      points -= 6 * pass.matchCount(/(.)\1/g)
      return points
    evaluatePassword = ->
      updatePb doEvaluatePassword mainpass.val()
    mainpass
    .unbind('change').change(evaluatePassword)
    .unbind('keydown').keydown(evaluatePassword)
    .unbind('keyup').keyup(evaluatePassword)
    evaluatePassword()

    generate.click -> generator.toggle(); $(this).toggleClass('active')
    doGenerate.click ->
      sets =
        lower: 'abcdefghijklmnopqrstuvwxyz'
        upper: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        numbers: '0123456789'
        symbols: '`~!@#$%^&*()_-+=[]{};:?,.<>'
      pos = for set, el of options
        if $(el).is(':checked') then sets[set] else null
      mainpass.val(pos.join('').randomize parseInt options.passlength.val() )

      evaluatePassword()
      mainpass.attr('type', 'password')
      showHide.removeClass('active').click()
      return false

