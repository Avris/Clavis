upload = $('#upload')
download = $('#download')
loading = $('#loading')
trash = $('#trash')
out = $('#filename')

keyId = $($('#attachment').parents('form')[0]).data('key-id')
download.attr('href', M.route('keyDownload', {id: keyId}))

trash.click ->
  return false if not confirm(M.l('Are you sure you want to delete an attachment for this key?'))
  upload.addClass('hidden')
  download.addClass('hidden')
  trash.addClass('hidden')
  loading.removeClass('hidden')
  $.post(M.route('keyRemoveAttachment', {id: keyId}), [], (data)->
    upload.removeClass('hidden')
    loading.addClass('hidden')
    out.val('[empty]')
  )
  return false

attachmentUploader = new plupload.Uploader({
    runtimes: 'html5,flash,silverlight,html4',
    browse_button: 'upload',
    drop_element: 'attachment',
    url: M.route('keyAttach', {id: keyId}),
    flash_swf_url: M.asset('lib/plupload/Moxie.swf'),
    silverlight_xap_url:  M.asset('lib/plupload/Moxie.xap'),
    multi_selection: false,
    init:
      FilesAdded: (up, files) ->
        upload.addClass('hidden')
        download.addClass('hidden')
        trash.addClass('hidden')
        loading.removeClass('hidden')
        attachmentUploader.start();
      FileUploaded: (up, file, info) ->
        loading.addClass('hidden')
        upload.removeClass('hidden')
        res = JSON.parse(info.response);
        if res.error
          out.val(res.error.message)
        else
          out.val(res.filename)
          download.removeClass('hidden')
          trash.removeClass('hidden')
      Error: (up, err) ->
        loading.addClass('hidden')
        upload.removeClass('hidden')
        out.val(err.message)
  })
attachmentUploader.init()