// Generated by CoffeeScript 1.8.0
(function() {
  var $tree, addKey, bind, createFolder, editFolder, loadFolder, mainContent, mainPreloader, modalFolder, modalKey, modalKeyChanges, reloadTree, removeFolder, resize, treeoptions, unrememberPassword;

  mainPreloader = $('.preloader');

  mainContent = $('main .content');

  modalFolder = $('#modal-folder');

  modalKey = $('#modal-key');

  modalKeyChanges = $('#modal-key-changes');

  treeoptions = {
    plugins: ['types', 'wholerow'],
    types: window.icons,
    core: {
      check_callback: true,
      multiple: false
    }
  };

  if (window.isAdmin) {
    treeoptions.plugins.push('contextmenu');
    treeoptions.contextmenu = {
      select_node: false,
      items: function(o, cb) {
        return {
          newkey: {
            label: "Create key",
            icon: "fa fa-plus",
            action: function(data) {
              var inst, obj;
              inst = $.jstree.reference(data.reference);
              obj = inst.get_node(data.reference);
              return addKey(obj.id === 'personal' ? '' : obj.id);
            }
          },
          create: {
            label: "Create subfolder",
            icon: "fa fa-folder",
            _disabled: function(data) {
              var inst, obj;
              inst = $.jstree.reference(data.reference);
              obj = inst.get_node(data.reference);
              return obj.id === 'personal';
            },
            action: function(data) {
              var inst, obj;
              inst = $.jstree.reference(data.reference);
              obj = inst.get_node(data.reference);
              return createFolder(obj.id);
            }
          },
          edit: {
            label: "Edit",
            icon: "fa fa-pencil",
            _disabled: function(data) {
              var inst, obj;
              inst = $.jstree.reference(data.reference);
              obj = inst.get_node(data.reference);
              return obj.id === 'personal';
            },
            action: function(data) {
              var inst, obj;
              inst = $.jstree.reference(data.reference);
              obj = inst.get_node(data.reference);
              return editFolder(obj.id);
            }
          },
          remove: {
            label: 'Delete',
            icon: "fa fa-trash",
            _disabled: function(data) {
              var inst, obj;
              inst = $.jstree.reference(data.reference);
              obj = inst.get_node(data.reference);
              return obj.id === 'personal' || obj.parent === '#';
            },
            action: function(data) {
              var inst, obj;
              inst = $.jstree.reference(data.reference);
              obj = inst.get_node(data.reference);
              return removeFolder(inst.is_selected(obj) ? inst.get_selected().join('') : obj.id);
            }
          }
        };
      }
    };
  }

  $tree = $('#jstree').jstree(treeoptions).on('changed.jstree', function(e, data) {
    if (data && data.selected && data.selected.length) {
      return loadFolder(data.selected.join(''));
    }
  });

  loadFolder = function(id) {
    mainPreloader.show();
    return $.get(M.route('folder', {
      id: id
    }), function(data) {
      mainContent.html(data).show();
      mainPreloader.hide();
      return bind();
    });
  };

  reloadTree = function() {
    var selected;
    selected = $tree.jstree('get_selected').join('');
    return $tree.jstree('deselect_all').jstree('select_node', selected);
  };

  editFolder = function(id) {
    mainPreloader.show();
    return $.get(M.route('folderEdit', {
      id: id
    }), [], function(data) {
      mainPreloader.hide();
      modalFolder.html(data).modal();
      setTimeout((function() {
        return modalFolder.find('input[type=text]')[0].focus();
      }), 500);
      return bind();
    });
  };

  removeFolder = function(id) {
    if (!confirm(M.l('Are you sure you want to remove this folder, its subfolders and keys?'))) {
      return false;
    }
    mainPreloader.show();
    return $.post(M.route('folderRemove', {
      id: id
    }), [], function(data) {
      return window.location.reload();
    });
  };

  createFolder = function(parent) {
    mainPreloader.show();
    return $.get(M.route('folderAdd', {
      id: parent
    }), [], function(data) {
      mainPreloader.hide();
      modalFolder.html(data).modal();
      setTimeout((function() {
        return modalFolder.find('input[type=text]')[0].focus();
      }), 500);
      return bind();
    });
  };

  unrememberPassword = function() {
    var pass, user;
    user = $('#key_username');
    pass = $('#key_password');
    if (!user.attr('value')) {
      user.val('');
    }
    if (!pass.attr('value')) {
      return pass.val('');
    }
  };

  addKey = function(folder) {
    mainPreloader.show();
    $.get(M.route('keyAdd', {
      id: folder
    }), function(data) {
      mainPreloader.hide();
      modalKey.html(data).modal();
      setTimeout((function() {
        modalKey.find('input[type=text]')[0].focus();
        return unrememberPassword();
      }), 500);
      return bind();
    });
    return false;
  };

  bind = function() {
    var checkUserRole, dbHider, folderForm, keyForm, permissionForm, userRole;
    $('a.folder-link').unbind('click').click(function() {
      $tree.jstree('deselect_all').jstree('select_node', $(this).data('folder-id'));
      return false;
    });
    $('a.key-link').unbind('click').click(function() {
      mainPreloader.show();
      $.get(M.route($(this).data('action'), {
        id: $(this).data('key-id')
      }), function(data) {
        mainPreloader.hide();
        modalKey.html(data).modal();
        setTimeout((function() {
          modalKey.find('input[type=text]')[0].focus();
          return unrememberPassword();
        }), 500);
        return bind();
      });
      return false;
    });
    $('.key-changes-link').unbind('click').click(function() {
      mainPreloader.show();
      $.get(M.route('keyChanges', {
        id: $(this).data('key-id')
      }), function(data) {
        modalKeyChanges.html(data).modal();
        mainPreloader.hide();
        return bind();
      });
      return false;
    });
    $('[data-toggle=tooltip]').tooltip();
    keyForm = modalKey.find('form');
    keyForm.unbind('submit').submit(function(e) {
      if (keyForm.isValid()) {
        mainPreloader.show();
        $.post(keyForm.attr('action'), keyForm.serializeObject(), function(data) {
          mainPreloader.hide();
          if (data === 'OK') {
            modalKey.modal('hide');
            return reloadTree();
          } else {
            modalKey.html(data);
            return bind();
          }
        });
      }
      e.preventDefault();
      return false;
    });
    $('.key-add').unbind('click').click(function() {
      return addKey($(this).data('folder-id'));
    });
    $('.folder-edit').unbind('click').click(function() {
      return editFolder($(this).data('folder-id'));
    });
    $('.folder-add').unbind('click').click(function() {
      return createFolder($(this).data('folder-id'));
    });
    $('.folder-remove').unbind('click').click(function() {
      return removeFolder($(this).data('folder-id'));
    });
    folderForm = modalFolder.find('form');
    folderForm.unbind('submit').submit(function(e) {
      if (folderForm.isValid()) {
        mainPreloader.show();
        $.post(folderForm.attr('action'), folderForm.serializeObject(), function(data) {
          if (data === 'OK') {
            return window.location.reload();
          } else {
            modalFolder.html(data);
            return bind();
          }
        });
      }
      e.preventDefault();
      return false;
    });
    $('.select2').select2();
    $('.key-remove').unbind('click').click(function() {
      if (!confirm(M.l('Are you sure you want to remove this key?'))) {
        return false;
      }
      mainPreloader.show();
      return $.post(M.route('keyRemove', {
        id: $(this).data('key-id')
      }), [], function(data) {
        return window.location.reload();
      });
    });
    userRole = $('select[name="user[role]"]');
    if (userRole.length) {
      permissionForm = $('label[for$="_ownPermissionList"]').parent().parent();
      checkUserRole = function() {
        if (userRole.val() === 'ROLE_ADMIN') {
          return permissionForm.hide();
        } else {
          return permissionForm.show();
        }
      };
      userRole.unbind('change').change(checkUserRole);
      checkUserRole();
    }
    dbHider = function() {
      $('.dbHide').parent().parent().hide();
      return $('.' + $('#formdata_db_driver').val()).parent().parent().show();
    };
    $('#formdata_db_driver').change(dbHider);
    dbHider();
    return $('form.installer').submit(function() {
      if ($(this).isValid()) {
        return mainPreloader.show();
      }
    });
  };

  bind();

  resize = function() {
    return mainPreloader.height(window.innerHeight);
  };

  $(window).resize(resize);

  resize();

}).call(this);
