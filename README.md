## Clavis - Shared password storage ##

We all have lots of lots of passwords and access keys... Keeping them in a web browser or a window application is a good idea, but only until you need to share them with somebody.

Clavis can be installed on your PHP server (no 3rd party involved in storing your sensitive data). It lets you share your keys and be in control of what and with whom are you sharing.

It's easy, safe, comfortable and totally free!

### Instalation ###

To install Clavis, download its latest version from [clavis.avris.it](http://clavis.avris.it) and unzip it inside your web server public directory. Make sure that the domain points to `web` folder, and that `App/parameters.yml`, `cache` and `logs` are writable.

Now enter your website from the web browser and you should see the Clavis Installer. Just fill out all the fields and enjoy Clavis!

To get a developer version, use [Composer](https://getcomposer.org/) with:

	php composer.phar create-project avris/clavis

### Extending ###

Clavis is based on the [Micrus framework](http://micrus.avris.it), so check out [its documentation](https://gitlab.com/Avris/Micrus-main/wikis/home) for more information.

To change the content of emails sent by Clavis, just edit the `App/Locale/en.yml` file.  

To translate Clavis, create a new file in `App/Locale` based on `en.yml`, containing key-value pairs (like `Add key: Dodaj klucz`) and change your default locale in `App/config.yml`.

To use different encryption algorythm than the default one, add this service in `App/config.yml`:

    crypt:
      class: App\Service\MyCrypt
      parameters: [@config.parameters.secret, @app, @config.security]
  
And create file `App\Service\MyCrypt.php`:

    <?php
    namespace App\Service;
    
    use Avris\Micrus\Crypt;
    
    class MyCrypt extends Crypt
    {
        public function encrypt($encrypt)
        {
			// your code here
            return 'encrypted'; 
        }

        public function decrypt($decrypt)
        {
			// your code here
            return 'decrypted'; 
        }
    }

### Author ###

* ![Foto](http://avris.it/gfx/favicon.png)&nbsp;**Andrzej Prusinowski** [Avris.it](http://avris.it)
* ![Favicon](http://avris.it/dl/cc.png)&nbsp;Licence: [CC-BY](http://creativecommons.org/licenses/by/3.0/pl/)