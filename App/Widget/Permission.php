<?php
namespace App\Widget;

use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Widget\Widget;
use Avris\Micrus\Assert as Assert;
use Avris\Micrus\Form;
use App\Model\Permission as PermissionModel;

class Permission extends Widget
{

    private $permissions = array();

    private $staticSide;
    private $staticValue;
    private $dynamicSide;
    private $dynamics;

    public function __construct(Form $form, $name, array $options = array(), $asserts = array())
    {
        parent::__construct($form, $name, $options, $asserts);

        list($this->staticSide, $this->staticValue) = $this->options->get('staticSide');
        $this->dynamicSide = $this->options->get('dynamicSide');
        if (!$this->staticSide || !$this->dynamicSide) { throw new InvalidArgumentException('Options "staticSide" and "dynamicSide" are reqired'); }

        $this->dynamics = $this->dynamicSide == 'user' ? \R::findAll('user', 'role <> "ROLE_ADMIN"') : \R::findAll('folder', 'public = 0');

        $permissions = \R::findAll('permission', $this->staticSide.'_id = ?', array($this->staticValue->id));
        foreach ($permissions as $permission) {
            $this->permissions[$permission->{$this->dynamicSide}->id] = $permission->write;
        }
    }

    protected function getTemplate($widgetValue = null)
    {
        $out = '';

        foreach ($this->dynamics as $dynamic) {
            $value = isset($this->permissions[$dynamic->id]) ? ($this->permissions[$dynamic->id] ? 'write' : 'read') : '';
            $out .= '<div class="row">'.
                '<div class="col-lg-4"><div class="btn-group btn-group-justified" data-toggle="buttons">';
            foreach (PermissionModel::$permissionIcons as $permission => $icon) {
                $out .= '<label class="btn btn-default btn-sm '.($value == $permission ? 'active' : '').'">
                <input type="radio" name="{name}['.$dynamic->id.']" value="'.$permission.'" {attributes} ' . ( $value == $permission ? 'checked="checked"' : '') . '><span class="fa fa-'.$icon.'"></span>
                </label>';
            }
            $out .= '</div></div><div class="col-lg-8 permission-dynamic">'.$dynamic.'</div></div>';
        }

        return $out;
    }

    protected function getDefaultAssert()
    {
        return array();
    }

    public function valueFormToObject($value)
    {
        $out = array();
        foreach ($value as $dynamicId => $val) {
            if (!$val) { continue; }

            $permission = \R::dispense('permission');
            $permission->{$this->staticSide} = $this->staticValue;
            $permission->{$this->dynamicSide.'_id'} = $dynamicId;
            $permission->write = $val == 'write';
            $out[] = $permission;
        }

        return $out;
    }

}

