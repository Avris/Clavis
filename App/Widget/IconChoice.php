<?php
namespace App\Widget;

use Avris\Micrus\Widget\Widget;
use Avris\Micrus\Assert as Assert;
use App\Model\HasIcon;

class IconChoice extends Widget
{

    protected function getTemplate($widgetValue = null)
    {
        $out = '<div class="btn-group btn-group-justified" data-toggle="buttons">';
        foreach (HasIcon::$icons as $name => $class) {
            $out .= '<label class="btn btn-default btn-sm '.($name == $widgetValue ? 'active' : '').'">
                <input type="radio" name="{name}" value="'.$name.'" {attributes} ' . ( $name == $widgetValue ? 'checked="checked"' : '') . '><span class="'.$class.'"></span>
                </label>';
        }
        $out .= '</div>';
        return $out;
    }

    protected function getDefaultAssert()
    {
        $icons = array_keys(HasIcon::$icons);

        return new Assert\Choice(array_combine($icons, $icons), false);
    }

}

