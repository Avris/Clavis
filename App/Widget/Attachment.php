<?php
namespace App\Widget;

use Avris\Micrus\Widget\Widget;

class Attachment extends Widget
{
    protected function getTemplate($widgetValue = null)
    {
        return $this->form->getObject()->id ?
            '<div class="input-group" id="attachment">
                <a href="#" class="input-group-addon" id="upload"><span class="fa fa-upload"></span></a>
                <a href="#" class="input-group-addon'.($widgetValue ? '' : ' hidden').'" id="trash"><span class="fa fa-trash"></span></a>
                <a href="#" class="input-group-addon hidden" id="loading"><span class="fa fa-spinner fa-pulse"></span></a>
                <input class="{widget_class}" id="filename" value="'.($widgetValue ? htmlentities($widgetValue) : '[empty]').'" disabled/>
                <a href="#" class="input-group-addon'.($widgetValue ? '' : ' hidden').'" id="download"><span class="fa fa-download"></span></a>
            </div>' :
            '<input class="{widget_class}" id="filename" value="Option available after saving" disabled/>';
    }
}
