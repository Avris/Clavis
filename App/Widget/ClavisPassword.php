<?php
namespace App\Widget;

use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Widget\Widget;
use Avris\Micrus\Crypt;

class ClavisPassword extends Widget
{
    protected function getTemplate($widgetValue = null)
    {
        $generatorBtn = '<div class="input-group-addon generate"><span class="fa fa-random"></span></div>';
        $generator =
            '<div class="generator" style="display: none;">
                <div class="generator-option"><label><input type="checkbox" data-option="lower" checked/> abcd…</label></div>
                <div class="generator-option"><label><input type="checkbox" data-option="upper" checked/> ABCD…</label></div>
                <div class="generator-option"><label><input type="checkbox" data-option="numbers" checked/> 1234…</label></div>
                <div class="generator-option"><label><input type="checkbox" data-option="symbols"/> !@#$%…</label></div>
                <div class="generator-option"><input type="number" min="5" max="120" value="12" data-option="passlength"></div>
                <div class="generator-option"><button type="button" class="do-generate btn btn-default btn-block btn-sm"><span class="fa fa-random"></span></button></div>
            </div>';
        $strength = '<div class="progress"><div class="progress-bar progress-bar-danger" style="width: 0"></div></div>';

        return
            '<div class="password-widget">
                <div class="input-group">
                    <input id="{id}" name="{name}" type="password" value="{value}" class="{widget_class} mainpass" {asserts} {attributes}/>
                    <div class="input-group-addon showHide"><span class="fa fa-eye"></span></div>
                    '.($this->options->get('generator') ? $generatorBtn : '').'
                </div>
                '.($this->options->get('strength') ? $strength : '').'
                '.($this->options->get('generator') ? $generator : '').'
            </div>';
    }

    public function valueFormToObject($value)
    {
        /** @var Crypt $crypt */
        $crypt = $this->options->get('crypt');
        return $crypt ? $crypt->encrypt($value) : $value;
    }

    public function valueObjectToForm($value)
    {
        /** @var Crypt $crypt */
        $crypt = $this->options->get('crypt');
        return $crypt ? $crypt->decrypt($value) : $value;
    }

}
