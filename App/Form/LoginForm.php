<?php
namespace App\Form;

use Avris\Micrus\Form;
use Avris\Micrus\Assert as Assert;
use Avris\Micrus\FormStyle\Bootstrap2;

class LoginForm extends Form {

    public function configure()
    {
        $this
            ->setStyle(new Bootstrap2)
            ->add('credentials', 'ObjectValidator', array('callback' => array($this, 'checkCredentials')))
            ->add('username', 'Email', array('label' => '[[Email]]'), new Assert\NotBlank())
            ->add('password', 'Password', array('label' => '[[Password]]'), new Assert\NotBlank())
            ->add('rememberme', 'Checkbox', array('label' => '', 'sublabel' => '[[Remember me]]'))
        ;
        $this->object->rememberme = true;
    }

    public function getUser()
    {
        return \R::findOne('user', 'username = ?', array($this->object->username));
    }

    public function shouldRememberHim()
    {
        return (bool)$this->object->rememberme;
    }

    public function checkCredentials($user)
    {
        $dbUser = \R::findOne('user', 'username = ?', array($user->username));
        if (!$dbUser || !$this->options->get('crypt')->validate($user->password, $dbUser->password)) {
            return '[[Email or password invalid]]';
        }
        return true;
    }
}