<?php
namespace App\Form;

use Avris\Micrus\Form;
use Avris\Micrus\Assert as Assert;
use Avris\Micrus\FormStyle\Bootstrap2;
use App\Model\User;

class UserForm extends Form
{

    public function configure()
    {
        $this
            ->setStyle(new Bootstrap2)
            ->add('username', 'Email', array('label' => '[[Email]]'), new Assert\NotBlank())
            ->add('email_free', 'ObjectValidator', array('callback' => array($this, 'checkEmailFree')))
            ->add('role', 'Choice', array('label' => '[[Role]]', 'choices' => User::$roles), new Assert\NotBlank)
        ;

        if ($user = $this->options->get('user')) {
            $this->add('ownPermissionList', 'Permission', array(
                'label' => '[[Permissions]]',
                'staticSide' => array('user', $user),
                'dynamicSide' => 'folder'
            ));
        }
    }

    public function checkEmailFree($user)
    {
        $user = \R::findOne('user', 'username = ? AND id <> ?', array($user->username, $user->id));
        if ($user) { return '[[User with such email already exists]]'; }
        return true;
    }

    public function getUser($clearCsrf = true)
    {
        $object = parent::getObject($clearCsrf);
        $user = $this->options->get('user');
        if ($user && $user->id) { \R::trashAll(\R::findAll('permission', 'user_id = ?', array($user->id))); }

        return $object;
    }

}