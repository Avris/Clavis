<?php
namespace App\Form;

use Avris\Micrus\Form;
use Avris\Micrus\Assert as Assert;
use Avris\Micrus\FormStyle\Bootstrap2;

class RemindForm extends Form {

    public function configure()
    {
        $this
            ->setStyle(new Bootstrap2)
            ->add('exists', 'ObjectValidator', array('callback' => array($this, 'checkExists')))
            ->add('username', 'Email', array('label' => '[[Email]]'), new Assert\NotBlank())
        ;
        $this->object->rememberme = true;
    }

    public function getUser()
    {
        return \R::findOne('user', 'username = ?', array($this->object->username));
    }

    public function checkExists($user)
    {
        $dbUser = \R::findOne('user', 'username = ?', array($user->username));
        if (!$dbUser) { return '[[Email not found in the database]]'; }
        return true;
    }
}