<?php
namespace App\Form;

use Avris\Micrus\Form;
use Avris\Micrus\Assert as Assert;
use Avris\Micrus\FormStyle\Bootstrap;
use Avris\Micrus\Crypt;

class PasswordForm extends Form {

    public function configure()
    {
        $this
            ->setStyle(new Bootstrap)
            ->add('password', 'ClavisPassword', array('label' => '[[Change password]]'), new Assert\NotBlank())
        ;
        $this->object->password = '';
    }

    public function getUser(Crypt $crypt)
    {
        $this->object->password = $crypt->hash($this->object->password);
        return $this->object;
    }
}