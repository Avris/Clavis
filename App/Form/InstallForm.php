<?php
namespace App\Form;

use App\Service\Mailer;
use Avris\Micrus\Form;
use Avris\Micrus\Assert as Assert;
use Avris\Micrus\FormStyle\Bootstrap2;
use Avris\Micrus\IO\ConfigLoader;

class InstallForm extends Form
{

    public function configure()
    {
        $this
            ->setStyle(new Bootstrap2)
            ->with('[[Database]]')
                ->add('db_driver', 'Choice', array('label' => '[[Driver]]', 'choices' => array(
                    'mysql' => 'MySQL',
                    'pgsql' => 'PostgreSQL',
                    'sqlite' => 'SQLite',
                    'cubrid' => 'CUBRID',
                )), new Assert\NotBlank())
                ->add('db_correct', 'ObjectValidator', array('callback' => array($this, 'checkDbCorrect')))
                ->add('db_host', 'Text', array('label' => '[[Host]]', 'attr' => array('class' => 'dbHide mysql pgsql cubrid')))
                ->add('db_port', 'Number', array('label' => '[[Port]]', 'attr' => array('class' => 'dbHide cubrid')))
                ->add('db_name', 'Text', array('label' => '[[Database]]', 'attr' => array('class' => 'dbHide mysql pgsql cubrid')))
                ->add('db_user', 'Text', array('label' => '[[Username]]', 'attr' => array('class' => 'dbHide mysql pgsql cubrid')))
                ->add('db_pass', 'Text', array('label' => '[[Password]]', 'attr' => array('class' => 'dbHide mysql pgsql cubrid')))
                ->add('db_file', 'Text', array('label' => '[[Filename]]', 'attr' => array('class' => 'dbHide sqlite')))
            ->end()
            ->with('SMTP Connection')
                ->add('mailer_correct', 'ObjectValidator', array('callback' => array($this, 'checkMailerCorrect')))
                ->add('mailer_host', 'Text', array('label' => '[[Host]]'), new Assert\NotBlank())
                ->add('mailer_port', 'Number', array('label' => '[[Port]]'), array(new Assert\NotBlank(), new Assert\Min(1)))
                ->add('mailer_secure', 'Choice', array('label' => '[[Secure]]', 'choices' => array(
                    'false' => '',
                    'ssl' => 'SSL',
                    'ttl' => 'TTL',
                )), new Assert\NotBlank())
                ->add('mailer_username', 'Text', array('label' => '[[Username]]'), new Assert\NotBlank())
                ->add('mailer_password', 'Text', array('label' => '[[Password]]'), new Assert\NotBlank())
                ->add('mailer_sender', 'Email', array('label' => '[[Sender email]]'), new Assert\NotBlank())
            ->end()
            ->with('Admin account')
                ->add('admin_mail', 'Email', array('label' => '[[Email]]'), new Assert\NotBlank())
                ->add('admin_pass', 'ClavisPassword', array(
                        'label' => '[[Password]]',
                        'strength' => true,
                        'generator' => true,
                    ),
                    array(new Assert\NotBlank(), new Assert\MinLength(8)))
            ->end()
        ;

        $this->object->db_host = 'localhost';
        $this->object->db_name = 'clavis';
        $this->object->db_file = 'clavis.s3db';
        $this->object->mailer_smtp = true;
    }

    public function checkDbCorrect($data)
    {
        \R::$toolboxes = array();
        $loader = new ConfigLoader($this->options->get('app'));
        \R::$currentDB = '';
        $loader->configDatabase(array('database' => $this->getDatabaseParams($data)));
        if (!\R::testConnection()) { return '[[Cannot connect to the database]]'; }
        return true;
    }

    public function checkMailerCorrect($data)
    {
        $mailer = new Mailer($this->getMailerParams($data));
        if (!$mailer->testConnection()) { return '[[Cannot connect to SMTP server]]'; }
        return true;
    }

    public function getDatabaseParams($data)
    {
        $out = array();
        foreach (array('driver', 'host', 'port', 'name', 'user', 'pass', 'file') as $attr) {
            $out[$attr] = $data->{'db_'.$attr};
        }
        return $out;
    }

    public function getMailerParams($data)
    {
        $out = array();
        foreach (array('host', 'port', 'secure', 'username', 'password', 'sender') as $attr) {
            $out[$attr] = $data->{'mailer_'.$attr};
        }
        return $out;
    }
}