<?php
namespace App\Form;

use Avris\Micrus\Form;
use Avris\Micrus\Assert as Assert;
use Avris\Micrus\FormStyle\Bootstrap2;

class KeyForm extends Form {

    public function configure()
    {
        $this
            ->setStyle(new Bootstrap2)
            ->add('icon', 'IconChoice', array('label' => '[[Icon]]'), new Assert\NotBlank())
            ->add('name', 'Text', array('label' => '[[Name]]'), new Assert\NotBlank())
            ->add('username', 'Text', array('label' => '[[Username]]'))
            ->add('password', 'ClavisPassword', array('label' => '[[Password]]',
                'crypt' => $this->options->get('crypt'),
                'generator' => true,
                'strength' => true,
            ))
            ->add('url', 'Text', array('label' => '[[URL]]'))
            ->add('database', 'Text', array('label' => '[[Database]]'))
            ->add('notes', 'Textarea', array('label' => '[[Notes]]'))
            ->add('expires', 'DateTime', array('label' => '[[Expires]]'), array(new Assert\MinDate('now')))
            ->add('attachment', 'Attachment', array('label' => '[[Attachment]]'))
        ;
    }
}