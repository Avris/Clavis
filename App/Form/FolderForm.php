<?php
namespace App\Form;

use Avris\Micrus\Form;
use Avris\Micrus\Assert as Assert;
use Avris\Micrus\FormStyle\Bootstrap2;

class FolderForm extends Form
{
    public function configure()
    {
        $this
            ->setStyle(new Bootstrap2)
            ->add('icon', 'IconChoice', array('label' => '[[Icon]]'), new Assert\NotBlank())
            ->add('name', 'Text', array('label' => '[[Name]]'), new Assert\NotBlank())
            ->add('public', 'Checkbox', array('label' => '[[Public]]'))
        ;

        if ($folder = $this->options->get('folder')) {
            $this->add('ownPermissionList', 'Permission', array(
                'label' => '[[Permissions]]',
                'staticSide' => array('folder', $folder),
                'dynamicSide' => 'user'
            ));
        }
    }

    public function getFolder($clearCsrf = true)
    {
        $object = parent::getObject($clearCsrf);
        $folder = $this->options->get('folder');
        if ($folder && $folder->id) { \R::trashAll(\R::findAll('permission', 'folder_id = ?', array($folder->id))); }

        return $object;
    }
}