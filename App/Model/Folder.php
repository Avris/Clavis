<?php
namespace App\Model;

/**
 * @property int            id
 * @property string         icon
 * @property string         name
 * @property Folder[]       ownFolder
 * @property User[]         sharedUserList
 * @property Key[]          ownKeyList
 * @property Permission[]   ownPermissionList
 **/
class Folder extends HasIcon
{
    public function __toString()
    {
        return $this->name;
    }

    public function dispense()
    {
        $this->icon = 'key';
        $this->name = '';
    }

    /**
     * @param User $user
     * @return Folder[] array
     */
    public function getChildren($user)
    {
        $children = array();
        foreach ($this->ownFolder as $child) {
            if ($child->canUserRead($user)) {
                $children[] = $child;
            }
        }
        return $children;
    }

    /**
     * @return Folder|null
     */
    public function getParent()
    {
        return $this->folder;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canUserRead($user)
    {
        return $this->public || $user->isAdmin() || $this->doesUserHavePermission($user, false);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canUserWrite($user)
    {
        return $user->isAdmin() || $this->doesUserHavePermission($user, true);
    }

    /**
     * @param User $user
     * @return bool
     */
    private function doesUserHavePermission($user, $checkWrite)
    {
        foreach($this->ownPermissionList as $permission) {
            if ($permission->user->id == $user->id) {
                return $checkWrite ? $permission->write : true;
            }
        }

        return false;
    }

    /**
     * @param User $user
     * @return Key[]
     */
    public function getKeys($user)
    {
        $keys = $this->ownKeyList;
        foreach ($this->getChildren($user) as $child) {
            $keys = array_merge($keys, $child->getKeys($user));
        }
        return $keys;
    }

    public function remove()
    {
        \R::trashAll($this->ownKeyList);

        foreach ($this->ownFolder as $child) {
            $child->remove();
        }

        \R::trash($this);
    }
}
