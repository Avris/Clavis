<?php
namespace App\Model;

/**
 * @property int        id
 * @property User       user
 * @property int        keyid
 * @property string     changes
 * @property \DateTime  date
 */
class Keychange extends HasIcon
{
    public function __toString()
    {
        return $this->changes;
    }

    public function getChanges()
    {
        return json_decode($this->changes, true);
    }

}
