<?php
namespace App\Model;

use Avris\Micrus\Bean;

class Permission extends Bean
{
    public static $permissionIcons = array(
        '' => 'lock',
        'read' => 'eye',
        'write' => 'pencil',
    );

    public function __toString()
    {
        return $this->user . ' can ' . ($this->write ? 'read and write' : 'read') . ' ' . $this->folder;
    }

    /**
     * @return Folder
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getIcon()
    {
        return self::$permissionIcons[$this->write ? 'write' : 'read'];
    }
}
