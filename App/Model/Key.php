<?php
namespace App\Model;

/**
 * @property int        id
 * @property string     icon
 * @property string     name
 * @property string     username
 * @property string     password
 * @property string     url
 * @property string     database
 * @property string     notes
 * @property \DateTime  expires
 * @property string     attachemnt
 * @property Folder     folder
 * @property User       user
 */
class Key extends HasIcon
{
    private $changed = array();

    public function __toString()
    {
        return $this->name;
    }

    public function dispense()
    {
        $this->icon = 'key';
        $this->name = '';
    }

    public function getParent()
    {
        return $this->folder;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canUserRead($user)
    {
        return $this->user ?
            $this->user->id == $user->id :
            $this->folder->canUserRead($user);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canUserWrite($user)
    {
        return $this->user ?
            $this->user->id == $user->id :
            $this->folder->canUserWrite($user);
    }

    public function update()
    {
        $bean = $this->unbox();
        $this->changed = array();

        foreach (array('icon', 'name', 'username', 'password', 'url', 'database', 'notes', 'expires', 'attachment') as $attr) {
            if ($bean->hasChanged($attr)) {
                $this->changed[$attr] = $bean->$attr;
            }
        }
    }

    public function after_update()
    {
        if (count($this->changed)) {
            \R::store($this->dispenseChange($this->changed));
        }
    }

    public function delete()
    {
        \R::store($this->dispenseChange(array('deleted' => 'deleted')));
    }

    public function dispenseChange($changes)
    {
        $now = new \DateTime;
        $keyChange = \R::dispense('keychange');
        $keyChange->user = isset($_SESSION['_user']) ? $_SESSION['_user'] : null;
        $keyChange->keyid = $this->id;
        $keyChange->changes = json_encode($changes);
        $keyChange->date = $now->format('Y-m-d H:i:s');
        return $keyChange;
    }

    public function getChanges()
    {
        return \R::findAll('keychange', '`keyid` = ?', array($this->id));
    }

}
