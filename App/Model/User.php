<?php
namespace App\Model;

use Avris\Micrus\Bean;

/**
 * @property int        id
 * @property string     username
 * @property string     password
 * @property string     role
 * @property Folder[]   sharedFolderList
 */
class User extends Bean
{
    public static $roles = array(
        'ROLE_USER' => 'User',
        'ROLE_ADMIN' => 'Admin',
    );

    public function getRoles()
    {
        return array($this->role);
    }

    public function getRoleName()
    {
        return self::$roles[$this->role];
    }

    public function isAdmin()
    {
        return $this->role == 'ROLE_ADMIN';
    }

    public function __toString()
    {
        return $this->username;
    }

    public function getAvatar($size = 50)
    {
        return "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $this->username ) ) ) . "?d=wavatar&s=" . $size;
    }

    public function getPermissions()
    {
        return $this->ownPermissionList;
    }

    public function canReadFolder($matches)
    {
        $id = is_array($matches) ? $matches[1] : $matches;
        $folder = \R::findOne('folder', 'id = ?', array($id));
        return $folder->canUserRead($this);
    }

    public function canWriteFolder($matches)
    {
        $id = is_array($matches) ? $matches[1] : $matches;
        $folder = \R::findOne('folder', 'id = ?', array($id));
        return $folder->canUserWrite($this);
    }

    public function canReadKey($matches)
    {
        $id = is_array($matches) ? $matches[1] : $matches;
        $key = \R::findOne('key', 'id = ?', array($id));
        return $key->canUserRead($this);
    }

    public function canWriteKey($matches)
    {
        $id = is_array($matches) ? $matches[1] : $matches;
        $key = \R::findOne('key', 'id = ?', array($id));
        return $key->canUserWrite($this);
    }
}
