<?php
namespace App\Model;

use Avris\Micrus\Bean;

/**
 * @property string     icon
 */
abstract class HasIcon extends Bean
{
    public static $icons = array(
        'key' => 'fa fa-key',
        'db' => 'fa fa-database',
        'server' => 'fa fa-server',
        'user' => 'fa fa-user',
        'team' => 'fa fa-group',
        'company' => 'fa fa-suitcase',
        'book' => 'fa fa-book',
        'mail' => 'fa fa-envelope',
        'file' => 'fa fa-file',
        'home' => 'fa fa-home',
        'shop' => 'fa fa-shopping-cart',
        'secure' => 'fa fa-shield',
    );

    public function getIconClass()
    {
        return static::$icons[$this->icon];
    }
}