<?php
namespace App\Service;

use Avris\Micrus\Event\TwigExtensionEvent;
use App\Model\HasIcon;
use Avris\Micrus\Crypt;

class ClavisTwigExtension
{
    /** @var Crypt */
    private $crypt;

    public function __construct(Crypt $crypt)
    {
        $this->crypt = $crypt;
    }

    public function twigExtensionEvent(TwigExtensionEvent $event)
    {
        $event->getTwig()->addFilter(
            new \Twig_SimpleFilter('urlify', function($data) {
                $data = htmlentities($data);
                if (filter_var($data, FILTER_VALIDATE_EMAIL)) { return '<a href="mailto:'.$data.'" target="_blank">'.$data.'</a>'; }
                if (filter_var($data, FILTER_VALIDATE_URL)) { return '<a href="'.$data.'" target="_blank">'.$data.'</a>'; }
                return $data;
            }, array('is_safe' => array('html_attr')))
        );

        $event->getTwig()->addFilter(
            new \Twig_SimpleFilter('cleanLinks', function($data) {
                return preg_replace('#<a.*?>(.*?)</a>#i', '\1', $data);
            }, array('is_safe' => array('html_attr')))
        );

        $crypt = $this->crypt;
        $event->getTwig()->addFilter(
            new \Twig_SimpleFilter('decrypt', function($data) use ($crypt) {
                return $crypt->decrypt($data);
            }, array('is_safe' => array('html_attr')))
        );

        $event->getTwig()->addGlobal('clavisIcons', HasIcon::$icons);

        if (\R::testConnection() && \R::inspect()) {
            $event->getTwig()->addGlobal('root', \R::findOne('folder', 'folder_id is null'));
        }
    }
}