<?php
namespace App\Service;

use Avris\Micrus\Bag\ParameterBag;
use Avris\Micrus\Event\RequestEvent;
use Avris\Micrus\Http\RedirectResponse;
use Avris\Micrus\Router;

class InstallerListener
{
    private $installed;

    private $router;

    public function __construct($installed, Router $router)
    {
        $this->installed = $installed instanceof ParameterBag ? false : $installed;
        $this->router = $router;
    }

    public function requestEvent(RequestEvent $event)
    {
        if ($this->installed || $event->getRequest()->get('params')->get('action') == 'installerAction') { return; }

        $event->setResponse(new RedirectResponse($this->router->getUrl('installer')));
    }
}