<?php
namespace App\Service;

use Avris\Micrus\Bag\ParameterBag;
use Avris\Micrus\Http\Request;
use Avris\Micrus\Templater;

class Mailer {

    /** @var ParameterBag */
    private $options;

    /** @var string */
    private $sender;

    /** @var Templater */
    private $templater;

    public function __construct($options, Request $request = null, Templater $templater = null)
    {
        $this->options = $options;
        $this->sender = isset($options['sender']) ? $options['sender'] : null;
        if (!$this->sender && $request) { $this->sender = 'clavis@' . $request->getServer('HTTP_HOST'); }
        if (!$this->sender) { $this->sender = 'clavis@avris.it'; }
        $this->templater = $templater;
    }

    public function send($subject, $content, array $receivers)
    {
        $mail = $this->getMail();

        foreach ($receivers as $email => $name) {
            $mail->addAddress($email, $name);
        }

        $mail->isHTML(true);
        $mail->addEmbeddedImage(__DIR__ . '/../../web/gfx/logo.png', 'logo');
        $mail->Subject = $subject;
        $mail->Body    = $this->templater->render(array(
            '_view' => 'mail.html.twig',
            'body' => $content,
        ));

        if(!$mail->send()) {
            throw new \Exception('Unable to send an email: ' . $mail->ErrorInfo);
        }
    }

    public function testConnection()
    {
        return $this->getMail()->smtpConnect();
    }

    private function getMail()
    {
        $mail = new \PHPMailer;

        $mail->IsSMTP();
        $mail->Host = $this->options['host'];
        $mail->Port = $this->options['port'];
        if (@$this->options['secure']) { $mail->SMTPSecure = $this->options['secure']; }
        if (isset($this->options['username']) && $this->options['username']) {
            $mail->SMTPAuth = true;
            $mail->Username = $this->options['username'];
            $mail->Password = $this->options['password'];
        }

        $mail->From = $this->sender;
        $mail->FromName = 'Clavis';

        return $mail;
    }
}