<?php
namespace App\Service;

use Avris\Micrus\Event\RequestEvent;

class CleanupExpired
{
    public function requestEvent(RequestEvent $event)
    {
        if (!\R::testConnection() || !\R::inspect()) { return; }

        $now = new \DateTime();
        $expired = \R::findAll('key', 'expires IS NOT NULL AND expires < ?', array($now->format('Y-m-d H:i:s')));

        foreach ($expired as $key) {
            \R::trash($key);
        }
    }
}