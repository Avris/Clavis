<?php
namespace App\Controller;

use App\Form\InstallForm;
use App\Task\FixturesTask;
use Avris\Micrus\Controller;

class HomeController extends Controller {

    public function homeAction()
    {
        return $this->render();
    }

    public function installerAction()
    {
        if ($this->getParameter('installed')) { return $this->redirectToRoute('home'); }
        
        $rootDir = $this->getApp()->getRootDir();

        foreach (array('App/parameters.yml', 'cache', 'logs') as $path) {
            if (!is_writable($rootDir . '/' . $path)) {
                throw new \Exception(sprintf('"%s" is not writable. Please change its permissions', $path));
            }
        }

        if (!$this->getParameter('secret')) {
            file_put_contents($rootDir . '/App/parameters.yml', \Spyc::YAMLDump(array(
                'installed' => false,
                'secret' => hash('sha256', mt_rand()),
            )));
        }

        $form = new InstallForm(null, array(
            'app' => $this->getApp(),
        ));

        $form->bind($this->getPost());
        if ($form->isValid()) {

            $fixtures = new FixturesTask($this->getApp());
            $fixtures->load();

            $dbParams = $form->getDatabaseParams($form->getObject());
            $dbParams['freeze'] = true;

            switch ($dbParams['driver']) {
                case 'mysql':
                    \R::exec('SET FOREIGN_KEY_CHECKS=0');
                    $this->wipeAll();
                    \R::exec('SET FOREIGN_KEY_CHECKS=1');
                    break;
                case 'pgsql':
                    \R::exec('SET CONSTRAINTS ALL DEFERRED');
                    $this->wipeAll();
                    \R::exec('SET CONSTRAINTS ALL IMMEDIATE');
                    break;
                case 'sqlite':
                    \R::exec('PRAGMA foreign_keys = 0');
                    $this->wipeAll();
                    \R::exec('PRAGMA foreign_keys = 1');
                    break;
                default:
            }

            $admin = \R::dispense('user');
            $admin->username = $form->getObject()->admin_mail;
            $admin->password = $this->getService('crypt')->hash($form->getObject()->admin_pass);
            $admin->role = 'ROLE_ADMIN';
            $admin->token = '';
            \R::store($admin);

            $folder = \R::dispense('folder');
            $folder->icon = 'home';
            $folder->name = 'Home';
            $folder->public	= true;
            \R::store($folder);

            file_put_contents($rootDir . '/App/parameters.yml', \Spyc::YAMLDump(array(
                'installed' => true,
                'database' => $dbParams,
                'mailer' => $form->getMailerParams($form->getObject()),
                'secret' => $this->getParameter('secret'),
            )));

            $this->addFlash('success', $this->getService('localizator')->get('Configuration set up successfully'));
            return $this->redirectToRoute('login');
        }

        return $this->render(array('form' => $form));
    }


    private function wipeAll()
    {
        foreach (\R::inspect() as $table) {
            \R::wipe($table);
        }
    }

}