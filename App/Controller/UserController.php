<?php
namespace App\Controller;

use Avris\Micrus\Controller;
use App\Form\UserForm;
use Avris\Micrus\Exception\NotFoundException;
use Avris\Micrus\Http\Request;
use App\Form\PasswordForm;
use App\Model\User;
use App\Form\LoginForm;
use App\Form\RemindForm;

class UserController extends Controller
{

    public function loginAction()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        $form = new LoginForm(\R::dispense('user'), array(
            'localizator' => $this->getService('localizator'),
            'crypt' => $this->getService('crypt'),
        ));
        $form->bind($this->getPost());
        if ($form->isValid()) {
            $user = $form->getUser();
            $this->getService('crypt')->login($user, $form->shouldRememberHim());
            $url = $this->getRequest()->getGet('url');
            return $this->redirect($url ?
                $this->getRouter()->prependToUrl($url) :
                $this->generateUrl('home'));
        }
        return $this->render(array('form' => $form));
    }

    public function remindAction()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        $form = new RemindForm();
        $form->bind($this->getPost());
        if ($form->isValid()) {
            $user = $form->getUser();
            $token = sha1(uniqid());
            $user->token = $token;
            \R::store($user);
            $this->sendMail('mail.remind', $user->username, $token);
            $this->addFlash('success', $this->getService('localizator')->get('Password resetting email has been sent to you'));
            return $this->redirectToRoute('login');
        }
        return $this->render(array('form' => $form));
    }

    public function remindTokenAction($token)
    {
        $user = \R::findOne('user', 'token = ?', array($token));
        if (!$user) { throw new NotFoundException; }

        $password = $this->generatePassword();
        $user->token = null;
        $user->password = $this->getService('crypt')->hash($password);
        \R::store($user);

        $this->getService('crypt')->login($user, false);

        $this->addFlash('success', $this->getService('localizator')->get('Your password has been set to "%pass%". You can change it in the form below.', array('%pass%' => $password)));

        return $this->redirectToRoute('account');
    }

    public function accountAction()
    {
        $form = new PasswordForm($this->getUser());

        $form->bind($this->getPost());
        if ($form->isValid()) {
            $this->addFlash('success', $this->getService('localizator')->get(
                'Your password has been changed'));
            \R::store($form->getUser($this->getService('crypt')));
            return $this->redirectToRoute('account');
        }

        return $this->render(array(
            'form' => $form,
        ));
    }

    public function listAction()
    {
        return $this->render(array(
            'users' => \R::findAll('user'),
        ));
    }

    public function addAction()
    {
        $form = new UserForm(\R::dispense('user'));

        $form->bind($this->getPost());
        if ($form->isValid()) {
            $user = $form->getUser();
            $user->password = $this->getService('crypt')->hash($this->generatePassword());
            $user->token = sha1(uniqid());
            \R::store($user);

            $this->sendMail('mail.welcome', $user->username, $user->token);

            $this->addFlash('success', $this->getService('localizator')->get(
                'User has been created and their password sent to %email%',
                array('%email%' => $user->username)));

            return $this->redirectToRoute('users');
        }

        return $this->render(array(
            '_view' => 'User/form.html.twig',
            'form' => $form,
            'header' => 'Create new user',
        ));
    }

    public function editAction(User $user)
    {
        $form = new UserForm($user, array(
            'user' => $user,
        ));

        $form->bind($this->getPost());
        if ($form->isValid()) {
            \R::store($form->getUser());
            $this->addFlash('success', $this->getService('localizator')->get(
                'User data have been saved'));

            return $this->redirectToRoute('userEdit', array('id' => $user->id));
        }

        return $this->render(array(
            '_view' => 'User/form.html.twig',
            'form' => $form,
            'header' => 'Edit user data',
        ));
    }

    public function removeAction(Request $request, User $user)
    {
        if ($user->id == $this->getUser()->id) {
            $this->addFlash('danger', $this->getService('localizator')->get(
                'You cannot remove your own account'));

            return $this->redirectToRoute('users');
        }

        if ($request->isPost()) {
            \R::trashAll($user->ownKey);
            \R::trash($user);
            $this->addFlash('success', $this->getService('localizator')->get(
                'User account has been removed'));

            return $this->redirectToRoute('users');
        }

        return $this->render(array('user' => $user));
    }

    private function generatePassword()
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
    }

    private function sendMail($template, $email, $token)
    {
        $localizator = $this->getService('localizator');

        $this->getService('mailer')->send($localizator->get($template.'.subject'),
            $localizator->get($template.'.body', array(
                '%host%' => $this->getRequest()->getServer('HTTP_HOST'),
                '%email%' => $email,
                '%token%' => $token,
            )),
            array($email => $email)
        );
    }
}