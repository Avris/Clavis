<?php
namespace App\Controller;

use Avris\Micrus\Controller;
use Avris\Micrus\Http\Request;
use Avris\Micrus\Http\Response;
use Avris\Micrus\Exception\NotFoundException;
use App\Form\KeyForm;
use App\Model\Folder;
use App\Model\Key;

class KeyController extends Controller
{

    public function addAction(Folder $folder = null)
    {
        $key = \R::dispense('key');
        if ($folder) {
            $key->folder = $folder;
        } else {
            $key->user = $this->getUser();
        }
        return $this->form($key->box());
    }

    public function showAction(Key $key)
    {
        return $this->render(array(
            'key' => $key,
            'password' => $this->getService('crypt')->decrypt($key->password),
        ));
    }

    public function editAction(Key $key)
    {
        return $this->form($key);
    }

    private function form(Key $key)
    {
        $form = new KeyForm($key , array(
            'crypt' => $this->getService('crypt'),
        ));

        $form->bind($this->getPost());
        if ($form->isValid()) {
            \R::store($form->getObject());
            return new Response('OK');
        }

        return $this->render(array(
            '_view' => 'Key/form.html.twig',
            'key' => $key,
            'form' => $form,
        ));
    }

    public function changesAction(Key $key)
    {
        $users = array();
        foreach (\R::findAll('user') as $user) { $users[$user['id']] = $user['username']; }

        return $this->render(array(
            'key' => $key,
            'users' => $users,
        ));
    }

    public function attachAction(Request $request, Key $key)
    {
        if ($file = $request->getFiles('file')) {
            $this->backupOldAttachment($key);
            copy($file['tmp_name'], $this->getAttachmentPath($key));
            $key->attachment = $file['name'];
            \R::store($key);
            return $this->renderJson(array(
                'jsonrpc' => '2.0',
                'result' => null,
                'id' => 'id',
                'filename' => $file['name'],
                'original_filename' => $file['name'],
            ));
        }

        return $this->renderJson(array(
            'error' => 'No file sent',
        ));
    }

    public function downloadAction(Key $key)
    {
        $filename = $this->getAttachmentPath($key);
        if (!$key->attachment || !file_exists($filename)) { throw new NotFoundException('This key doesn\'t have any attachment'); }

        return new Response(file_get_contents($filename), 200, array(
            'Content-Disposition' => 'attachment; filename="'.$key->attachment.'"',
        ));
    }

    public function removeAttachmentAction(Key $key)
    {
        if (!$key->attachment) { throw new NotFoundException('This key doesn\'t have any attachment'); }

        $this->backupOldAttachment($key);
        @unlink($this->getAttachmentPath($key));
        $key->attachment = null;
        \R::store($key);

        return new Response('OK');
    }

    private function getAttachmentPath(Key $key)
    {
        return $this->getApp()->getRootDir() . '/attachments/' . $key->id;
    }

    private function backupOldAttachment(Key $key)
    {
        $filename = $this->getAttachmentPath($key);
        if (!file_exists($filename)) { return; }
        rename($filename, $filename . '_' . time('YmdHis'));
    }

    public function removeAction(Key $key)
    {
        \R::trash($key);
        return new Response('OK');
    }

}