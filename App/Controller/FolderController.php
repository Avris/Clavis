<?php
namespace App\Controller;

use Avris\Micrus\Controller;
use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Http\Response;
use App\Form\FolderForm;
use App\Model\Folder;

class FolderController extends Controller
{

    public function showPersonalAction()
    {
        return $this->render(array(
            'keys' => \R::find('key', 'user_id = ?', array($this->getUser()->id)),
        ));
    }

    public function showAction(Folder $folder)
    {
        return $this->render(array(
            'folder' => $folder,
            'personalkeys' => $folder->folder === null ? \R::find('key', 'user_id = ?', array($this->getUser()->id)) : array(),
        ));
    }

    public function addAction(Folder $parent)
    {
        $folder = \R::dispense('folder');
        $folder->folder = $parent;
        return $this->form($folder->box());
    }

    public function editAction(Folder $folder)
    {
        return $this->form($folder);
    }

    public function form(Folder $folder)
    {
        $form = new FolderForm($folder, array(
            'folder' => $folder->id ? $folder : null,
        ));

        $form->bind($this->getPost());
        if ($form->isValid()) {
            \R::store($form->getFolder());
            return new Response('OK');
        }

        return $this->render(array(
            '_view' => 'Folder/form.html.twig',
            'folder' => $folder,
            'form' => $form,
        ));
    }

    public function removeAction(Folder $folder)
    {
        if (!$folder->folder) { throw new InvalidArgumentException('Cannot delete root folder'); }
        $folder->remove();
        return new Response('OK');
    }
}