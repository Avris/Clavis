<?php

namespace App\Task;

class FixturesTask extends \Avris\Micrus\Task\FixturesTask
{
    public function load()
    {
        if ($this->output) { $this->output->writeln('Truncating the database'); }
        \R::nuke();

        $crypt = $this->getApp()->get('crypt');

        if ($this->output) { $this->output->writeln('Loading: users'); }
        $admin = \R::dispense('user');
        $admin->username = 'admin@clavis.avris.it';
        $admin->password = $crypt->hash('adminpass');
        $admin->role = 'ROLE_ADMIN';
        $admin->token = '';
        \R::store($admin);

        $user = \R::dispense('user');
        $user->username = 'user@clavis.avris.it';
        $user->password = $crypt->hash('userpass');
        $user->role = 'ROLE_USER';
        $user->token = '';
        \R::store($user);

        if ($this->output) { $this->output->writeln('Loading: folders'); }
        $folder = \R::dispense(array(
            '_type' => 'folder',
            'icon' => 'home',
            'name' => 'Home',
            'public' => true,
            'ownFolder' => array(
                array(
                    '_type' => 'folder',
                    'icon' => 'company',
                    'name' => 'Customers',
                    'public' => true,
                    'ownFolder' => array(
                        array(
                            '_type' => 'folder',
                            'icon' => 'company',
                            'name' => 'Customer 1',
                            'public' => false,
                            'ownKey' => array(
                                array(
                                    '_type' => 'key',
                                    'icon' => 'server',
                                    'name' => 'Production server',
                                    'username' => 'root',
                                    'password' => $crypt->encrypt('QPx5u7QJYtBB'),
                                    'url' => '10.0.0.123:22',
                                    'database' => '',
                                    'notes' => 'Access via VPN',
                                    'expires' => new \DateTime('+1 year'),
                                    'attachment' => null,
                                ),
                                array(
                                    '_type' => 'key',
                                    'icon' => 'shop',
                                    'name' => 'Webshop test user',
                                    'username' => 'tester@customer1.com',
                                    'password' => $crypt->encrypt('8q3rwksYFUJw'),
                                    'url' => 'http://shop.customer1.com',
                                    'database' => '',
                                    'notes' => '',
                                    'expires' => null,
                                    'attachment' => null,
                                ),
                            ),
                        ),
                        array(
                            '_type' => 'folder',
                            'icon' => 'company',
                            'name' => 'Customer 2',
                            'public' => false,
                        ),
                    ),
                ),
                array(
                    '_type' => 'folder',
                    'icon' => 'server',
                    'name' => 'Servers',
                    'public' => true,
                    'ownFolder' => array(
                        array(
                            '_type' => 'folder',
                            'icon' => 'server',
                            'name' => 'Dev',
                            'public' => false,
                        ),
                        array(
                            '_type' => 'folder',
                            'icon' => 'server',
                            'name' => 'Test',
                            'public' => false,
                        ),
                        array(
                            '_type' => 'folder',
                            'icon' => 'server',
                            'name' => 'Prod',
                            'public' => false,
                        ),
                    ),
                ),
            ),
        ));

        $permission = \R::dispense('permission');
        $permission->user = $user;
        $permission->folder = $folder->ownFolder[1]->ownFolder[0];
        $permission->write = false;
        \R::store($permission);

        $permission = \R::dispense('permission');
        $permission->user = $user;
        $permission->folder = $folder->ownFolder[1]->ownFolder[1];
        $permission->write = true;
        \R::store($permission);

        \R::store($folder);

        $key = \R::dispense(array(
            '_type' => 'key',
            'icon' => 'mail',
            'name' => 'Office email',
            'username' => 'admin@company.com',
            'password' => $crypt->encrypt('YJuIaG7Os6bC'),
            'url' => 'http://gmail.com',
            'database' => '',
            'notes' => '',
            'expires' => null,
            'attachment' => null,
            'user' => $admin,
        ));
        \R::store($key);

        if ($this->output) { $this->output->writeln('Fixtures loaded'); }
    }
}
